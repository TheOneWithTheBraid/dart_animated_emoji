import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:archive/archive_io.dart';

import 'src/animated_emoji_scheme.dart';

const notoEmojiApiUrl =
    'https://googlefonts.github.io/noto-emoji-animation/data/api.json';
const emojiBaseUrl = 'https://fonts.gstatic.com/s/e/notoemoji/latest/';

const dotLottiePath = 'generated/noto-animated.lottie';

final client = HttpClient();

Future<void> main(List<String> args) async {
  if (args.isEmpty || ['help', '-h', '--help'].contains(args[0])) {
    print(_helpText);
    return;
  }
  final outputPath = args[0];

  print('Generating Animated Emoji data at $outputPath in dotLottie format.');

  final request = await client.getUrl(Uri.parse(notoEmojiApiUrl));
  final response = await request.close();

  if (response.statusCode != HttpStatus.ok) throw (response);

  DateTime? lastModified;
  final lastModifiedHeader =
      response.headers.value(HttpHeaders.lastModifiedHeader);
  if (lastModifiedHeader != null) {
    lastModified = DateTime.tryParse(lastModifiedHeader);
  }

  final responseString = await response.transform(utf8.decoder).join();

  final json = jsonDecode(responseString);
  final emojiScheme = AnimatedEmojiScheme.fromJson(json);

  final supportedCodepoints =
      emojiScheme.icons.map((e) => e.codepoint).toList();

  final downloads = Map.fromEntries(await Future.wait(
    emojiScheme.icons
        .map(
          (e) => _downloadAnimatedEmoji(
            e.codepoint,
          ).then((data) => MapEntry(e.codepoint, data)),
        )
        .toList(),
  ));

  final archive = Archive();
  final animations = <Map<String, Object?>>[];

  downloads.forEach((codepoint, data) {
    final path = 'animations/$codepoint.json';
    archive.addFile(
      ArchiveFile(path, data.lengthInBytes, data),
    );
    animations.add({
      // Name of the Lottie animation file without .json
      "id": codepoint,
      // Desired playback speed
      "speed": 1.0,
      // Whether to loop or not
      "loop": true,
    });
  });

  final emojiListFile = File(outputPath);
  await emojiListFile.create(recursive: true);
  await emojiListFile.writeAsString(
    '''
// generated file. Do not edit. Check `bin/generator.dart` for further information.

part of '../animated_emoji.dart';

const _supportedCodePoints = ${jsonEncode(supportedCodepoints)};

const _all = [
${emojiScheme.icons.map((e) => '''
  AnimatedEmoji._(
    name: '${e.name}',
    popularity: ${e.popularity},
    codepoints: '${e.codepoint}',
    categories: ${jsonEncode(e.categories)},  
    tags: ${jsonEncode(e.tags.map((e) => e.replaceAll(':', '')).toList())},
  ),
''').toList().join()}
];

''',
    mode: FileMode.writeOnly,
    flush: true,
  );

  print('Downloaded ${emojiScheme.icons.length} animated emojis.');

  final manifest = utf8.encode(jsonEncode({
    // Name and version of the software that created the dotLottie
    // kep in sync with pubspec.yaml
    "generator": "dart_animated_emoji 0.1.0",
    // Target dotLottie version
    "version": 1.0,
    // Revision version number of the dotLottie
    // kep in sync with pubspec.yaml
    "revision": 1,
    // Name of the author
    "author": "Google Inc.",
    // List of animations
    "animations": animations,
  }));
  archive.addFile(
    ArchiveFile('manifest.json', manifest.lengthInBytes, manifest),
  );

  final file = File('lib/$dotLottiePath');
  await file.create(recursive: true);

  final bytes = ZipEncoder().encodeBytes(
    archive,
    level: DeflateLevel.bestCompression,
    autoClose: true,
    modified: lastModified,
  );

  await file.writeAsBytes(bytes, flush: true);

  print('Created dotLottie font at lib/$dotLottiePath.');
}

Future<Uint8List> _downloadAnimatedEmoji(String codepoint) async {
  final url = _getAnimatedDataUrl(codepoint);

  final request = await client.getUrl(url);
  final response = await request.close();

  if (response.statusCode != HttpStatus.ok) throw (response);

  final views = await response.toList();
  final buffer = <int>[];
  for (final e in views) {
    buffer.addAll(e);
  }

  return Uint8List.fromList(buffer);
}

Uri _getAnimatedDataUrl(String codepoint) =>
    Uri.parse(emojiBaseUrl).resolve('$codepoint/lottie.json');

const _helpText = '''
Noto Animated Emoji generator for dart.

Usage :

dart_animated_emoji:generator path

- path   : the output file to write the generated code to.
           e.g. `lib/src/generated/emoji_list.g.dart`

Further documentation about Noto Animated Emoji font, animation formats and
asset licenses :

https://googlefonts.github.io/noto-emoji-animation/

This project is in no way affiliated with Noto.

Author : The one with the braid <info@braid.business>
''';
