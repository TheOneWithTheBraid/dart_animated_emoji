class AnimatedEmojiScheme {
  final String host;
  final String assetUrlPattern;
  final List<String> families;
  final List<AnimatedEmoji> icons;

  const AnimatedEmojiScheme({
    required this.host,
    required this.assetUrlPattern,
    required this.families,
    required this.icons,
  });

  factory AnimatedEmojiScheme.fromJson(Map<String, dynamic> json) =>
      AnimatedEmojiScheme(
        host: json['host'],
        assetUrlPattern: json['asset_url_pattern'],
        families: List<String>.from(json['families']),
        icons: (json['icons'] as List)
            .map((i) => AnimatedEmoji.fromJson(i))
            .toList(),
      );
}

class AnimatedEmoji {
  final String name;
  final int version;
  final int popularity;
  final String codepoint;
  final List<Object> unsupportedFamilies;
  final List<String> categories;
  final List<String> tags;
  final List<Object> sizesPx;

  const AnimatedEmoji({
    required this.name,
    required this.version,
    required this.popularity,
    required this.codepoint,
    required this.unsupportedFamilies,
    required this.categories,
    required this.tags,
    required this.sizesPx,
  });

  factory AnimatedEmoji.fromJson(Map<String, dynamic> json) => AnimatedEmoji(
        name: json['name'],
        version: json['version'],
        popularity: json['popularity'],
        codepoint: json['codepoint'],
        unsupportedFamilies: List<Object>.from(json['unsupported_families']),
        categories: List<String>.from(json['categories']),
        tags: List<String>.from(json['tags']),
        sizesPx: List<Object>.from(json['sizes_px']),
      );
}
