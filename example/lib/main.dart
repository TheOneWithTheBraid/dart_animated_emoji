import 'package:dart_animated_emoji/dart_animated_emoji.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.pink),
        useMaterial3: true,
      ),
      home: const AnimationHomePage(),
    );
  }
}

class AnimationHomePage extends StatefulWidget {
  const AnimationHomePage({super.key});

  @override
  State<AnimationHomePage> createState() => _AnimationHomePageState();
}

class _AnimationHomePageState extends State<AnimationHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Animations'),
      ),
      body: GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
        itemCount: AnimatedEmoji.all.length,
        itemBuilder: (context, index) {
          return Center(
            child: SizedBox.square(
              dimension: 128,
              child: Tooltip(
                message: AnimatedEmoji.all[index].tags.join(', '),
                child: LottieBuilder.asset(
                  AnimatedEmoji.flutterNotoDotLottieAsset,
                  decoder: (bytes) => LottieComposition.decodeZip(
                    bytes,
                    filePicker: AnimatedEmoji.all[index].archiveFilePicker,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
