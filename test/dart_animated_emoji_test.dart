import 'package:dart_animated_emoji/dart_animated_emoji.dart';
import 'package:test/test.dart';

void main() {
  test('Check search by tag', () {
    final unicorn = AnimatedEmoji.all.firstWhere(
      (element) => element.tags.contains('unicorn'),
    );
    expect(unicorn.codepoints, '1f984');
    expect(unicorn.fallback, '🦄');
  });
  test('Check joined emoji', () {
    final thumbsUp = AnimatedEmoji.fromGlyph('👍🏽');
    expect(thumbsUp?.codepoints, '1f44d_1f3fd');
  });
  test('Check isSupported method', () {
    expect(AnimatedEmoji.isEmojiSupported('🥺'), isTrue);
    expect(AnimatedEmoji.isEmojiSupported('👍🏽'), isTrue);
  });
}
