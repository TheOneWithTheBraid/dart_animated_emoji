# dart_animated_emoji

A pure dart representation of Noto Animated Emoji font for efficient animation rendering.

Does not depend on Flutter.

<img src="preview.webp" alt="An animated unicorn with a rainbow" width="128"/>

## Features

- render as efficient Lottie
- ship unified, compressed [dotLottie](https://dotlottie.io/) animation font for easy Flutter use
- check supported Unicode Glyphs
- tiny and minimal dependencies
- does not depend on Flutter

## Usage

If you are using Flutter, add the dotLottie font to your `pubspec.yaml` :

```yaml
flutter:
  assets:
    - packages/dart_animated_emoji/generated/noto-animated.lottie
```

```dart
void main() {
  // check whether a particular emoji glyph is supported
  final isThumbsUpSupported = AnimatedEmoji.isEmojiSupported('👍🏽');

  final thumb = AnimatedEmoji.fromGlyph('👍🏽');

  // search for a supported emoji emoji by a tag
  final unicorn = AnimatedEmoji.all.firstWhere(
        (element) => element.tags.contains('unicorn'),
  );

  // render as Flutter widget using `lottie` package
  LottieBuilder.asset(
    AnimatedEmoji.flutterNotoDotLottieAsset,
    decoder: (bytes) =>
        LottieComposition.decodeZip(
          bytes,
          filePicker: unicorn.archiveFilePicker,
        ),
  );
}
```

For further documentation about Flutter rendering, check out the example or the [`package:lottie`](https://pub.dev/packages/lottie).

## Package development

```shell
# update the generated metadata and the dotLottie font
dart run dart_animated_emoji:generator lib/src/generated/emoji_list.g.dart

# additional information
dart run dart_animated_emoji:generator --help
```

## Additional information

This project is in no way affiliated with the Noto authors. The Noto Emoji Animated font is licensed as CC BY-4.0.
