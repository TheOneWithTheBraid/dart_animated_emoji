import 'package:archive/archive.dart';

part 'generated/emoji_list.g.dart';

/// represents an Animated Emoji from Noto font.
class AnimatedEmoji {
  final String name;
  final int popularity;
  final String codepoints;
  final List<String> categories;
  final List<String> tags;

  const AnimatedEmoji._({
    required this.name,
    required this.popularity,
    required this.codepoints,
    required this.categories,
    required this.tags,
  });

  /// check whether a particular glyph is supported by animated emoji and
  /// returns the corresponding [AnimatedEmoji] if supported.
  /// returns [null] when unsupported;
  static AnimatedEmoji? fromGlyph(String glyph) => AnimatedEmoji.all
      .where(
        (emoji) => emoji.fallback == glyph,
      )
      .firstOrNull;

  /// check whether a particular glyph is supported by animated emoji
  static bool isEmojiSupported(String glyph) => AnimatedEmoji.all.any(
        (emoji) => emoji.fallback == glyph,
      );

  /// The path of the generated `noto-animated.lottie` file relative to the package root
  ///
  /// This file is generated from Noto's Emoji metadata and contains all animated glyphs.
  /// For Flutter, rather use [flutterNotoDotLottieAsset] for the Asset loader.
  ///
  /// Further read:
  /// - https://dotlottie.io/
  static const dotLottiePath = 'lib/generated/noto-animated.lottie';

  /// The Flutter Asset path of the generated `noto-animated.lottie`
  ///
  /// This file is generated from Noto's Emoji metadata and contains all animated glyphs.
  /// For Flutter, rather use [flutterNotoDotLottieAsset] for the Asset loader.
  ///
  /// This is just a convenient helper for Flutter use.
  ///
  /// Further read:
  /// - https://dotlottie.io/
  static const flutterNotoDotLottieAsset =
      'packages/dart_animated_emoji/generated/noto-animated.lottie';

  /// List of all supported Unicode codepoints in Noto Animated Emoji
  ///
  /// Joint code points are represented with an underscore `_`
  /// E.g. `thumbs-up` : `1f44d_1f3ff`
  static const List<String> supportedCodePoints = _supportedCodePoints;

  /// List of all [AnimatedEmoji] instances supported
  static final all = _all;

  /// picks the correct Lottie json file from the generated dotLottie font
  ///
  /// This is useful when using with [`package:lottie`](https://pub.dev/packages/lottie)
  ///
  /// Further read:
  /// - https://pub.dev/documentation/lottie/latest/lottie/LottieComposition/decodeZip.html
  /// - https://pub.dev/packages/lottie#telegram-stickers-tgs-and-dotlottie-lottie
  ArchiveFile archiveFilePicker(List<ArchiveFile> files) => files.firstWhere(
        (f) => f.name == 'animations/$codepoints.json',
      );

  /// the fallback unicode emoji to render
  ///
  /// Also useful for search
  String get fallback => String.fromCharCodes(
        codepoints.split('_').map(
              (c) => int.parse(c, radix: 16),
            ),
      );
}
