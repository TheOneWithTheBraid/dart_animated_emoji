## 0.1.2

* fix codestyle

## 0.1.1

* fix pubignore

## 0.1.0

* migrate to dotLottie

## 0.0.3

* fix generated files

## 0.0.2

* update to current Noto character set
* update dependencies
* support AVIF

## 0.0.1

* initial release
